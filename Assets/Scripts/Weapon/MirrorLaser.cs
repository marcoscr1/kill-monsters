using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorLaser : MonoBehaviour
{
    /*
        COntrolador de la mira laser
    */


    LineRenderer ln;

    Vector3 startPoint, endPoint, directionLaser;
    [SerializeField] private float longMaxLaser = 50f;

    void Start()
    {
        ln = GetComponent<LineRenderer>();
    }

   
    void Update()
    {
        PositionLaser();
    }

    void PositionLaser()
    {
        startPoint = transform.position;
        directionLaser = transform.forward;

        Ray laser = new Ray(startPoint, directionLaser);

        if (Physics.Raycast(laser, out RaycastHit hit, longMaxLaser))
        {
            ln.SetPosition(0, startPoint);
            ln.SetPosition(1, hit.point);
        }
        else
        {
            ln.SetPosition(0, startPoint);
            ln.SetPosition(1, startPoint + directionLaser * longMaxLaser);
        }
    }
}
