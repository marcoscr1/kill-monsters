using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    /*
        Controlador del arma del jugador
    */

    [Header("SFX")]
    AudioSource audioSource;
    [SerializeField] private AudioClip SFXShoot;

    // Valores del retroceso del arma
    const float RECOIL_FORCE = 4f;
    const float RECOIL_DELAY = 0.1f;
    const float RETURN_DELAY = 0.5f;
    const float LERP_SPEED = 5f;


    [SerializeField] private GameObject bulletController;

    bool canShoot = true;

    Vector3 originalPosition;


    void Start()
    {
        originalPosition = transform.localPosition;
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1") && canShoot && Time.timeScale != 0)
        {
            StartCoroutine(Shoot());
        }
    }


    IEnumerator Shoot()
    {
        canShoot = false;

        // Disparo del arma
        bulletController.GetComponent<BulletController>().Shoot();

        audioSource.PlayOneShot(SFXShoot);

        // Se aplica el retroceso del arma
        ApplyRecoil();

        yield return new WaitForSeconds(RECOIL_DELAY);

        // Recupera la posicion anterior del retroceso
        StartCoroutine(ToOriginalPosition());
    }



    void ApplyRecoil()
    {
        transform.localPosition = transform.localPosition - transform.forward * (RECOIL_FORCE / 50f);
    }


    IEnumerator ToOriginalPosition()
    {
        float t = 0;

        Vector3 actualPosition = transform.localPosition;

        while (t < RETURN_DELAY)
        {
            transform.localPosition = Vector3.Lerp(actualPosition, originalPosition, t / RETURN_DELAY);
            t += Time.deltaTime * LERP_SPEED;
            yield return null;
        }

        transform.localPosition = originalPosition;
        canShoot = true;
    }

}
