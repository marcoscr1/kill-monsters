using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class Bullet : MonoBehaviour
{
    /*
        Controlador de la bala
    */

    const float SPEED_BULLET = 50f;
    const float LIFE_BULLET = 2f;


    void Start()
    {
        Destroy(gameObject, LIFE_BULLET);

    }


    void Update()
    {
        transform.Translate(Vector3.forward * SPEED_BULLET * Time.deltaTime);
    }


    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("World"))
        {
            Destroy(gameObject);
        }
    }
}
