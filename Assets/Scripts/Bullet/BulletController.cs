using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class BulletController : MonoBehaviour
{
    /*
        Controlador de como efectúa el disparo del arma
    */

    const float DELAY_SHOOT = 0.6f;
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject FXShoot;


    public void Shoot()
    {
        // Efecto disparo
        GameObject flashShoot = Instantiate(FXShoot, transform.position, Quaternion.Euler(transform.forward));
        Destroy(flashShoot, 0.5f);

        // Creación de la bala
        GameObject cloneBullet = Instantiate(bullet, transform.position, transform.rotation);
    }
}
