using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class SoldierAnim : MonoBehaviour
{
    NavMeshAgent agent;
    [SerializeField] Animator anim;


    GameObject player;


    [Header("Weapons")]
    [SerializeField] private GameObject weapon;
    [SerializeField] private GameObject weaponController;


    [Header("Speed Zombie")]
    [SerializeField] float speedPoint; // 0.3
    [SerializeField] float speedRunToPlayer; // 2


    [Header("Paths")]
    [SerializeField] private GameObject[] pathPoint;
    float distanceToChangePath = 5f;
    int indexPath = 0;


    [Header("Distances")]
    [SerializeField] private float distanceToFollowPlayer;
    [SerializeField] private float distanceToAttackPlayer;
    float distanceToPlayer;


    [Header("SFX")]
    AudioSource audioSource;
    [SerializeField] private AudioClip SFXDeath;
    [SerializeField] private AudioClip SFXDamage;
    [SerializeField] private GameObject blood;


    SoldierController soldierController;


    void Start()
    {
        // Obtenemos las referencias de componentes y GameObjects
        agent = GetComponent<NavMeshAgent>();

        player = GameObject.Find("Player");

        audioSource = GetComponent<AudioSource>();

        soldierController = GetComponent<SoldierController>();

        // Ajustamos la distancia donde para el zombie
        agent.stoppingDistance = distanceToAttackPlayer;

    }


    void Update()
    {
        distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);

        if (distanceToPlayer <= distanceToAttackPlayer && !soldierController.GetIsDeath())
        {
            AttackToPlayer();
        }
        else if (distanceToPlayer <= distanceToFollowPlayer && !soldierController.GetIsDeath())
        {
            FollowToPlayer();
        }
        else if (pathPoint.Length > 0 && pathPoint != null && !soldierController.GetIsDeath())
        {
            FollowToPoint();
        }

        // Si el soldado muere, llama al método DropTheWeapon para que caiga el arma
        if (GetComponent<SoldierController>().GetIsDeath())
        {
            Invoke("DropTheWeapon", 0.8f);
        }
    }


    // Método cuando el soldado no detecta al jugador y está andando dirección a los puntos establecidos
    void FollowToPoint()
    {
        anim.SetBool("isRun", false);
        anim.SetBool("isShoot", false);

        agent.speed = speedPoint;

        agent.SetDestination(pathPoint[indexPath].transform.position);
        agent.stoppingDistance = 0;

        weaponController.GetComponent<SoldierShoot>().StopShoot();

        if (Vector3.Distance(transform.position, pathPoint[indexPath].transform.position) <= distanceToChangePath)
        {
            indexPath = ((indexPath + 1) == pathPoint.Length) ? 0 : indexPath + 1;
        }
    }


    // Método cuando el soldado está siguiendo al jugador
    void FollowToPlayer()
    {
        Reappoint();

        anim.SetBool("isRun", true);
        anim.SetBool("isShoot", false);

        agent.speed = speedRunToPlayer;

        agent.SetDestination(player.transform.position);
        agent.stoppingDistance = distanceToAttackPlayer;

        weaponController.GetComponent<SoldierShoot>().setShootRun(true);
    }


    // Método cuando el soldado se detiene y dispara al jugador (aumenta la velocidad del disparo)
    void AttackToPlayer()
    {
        Reappoint();

        anim.SetBool("isRun", false);
        anim.SetBool("isShoot", true);

        weaponController.GetComponent<SoldierShoot>().SetShootIdle(true);
    }


    // Método cuando el soldado sufre un daño
    public void DamageEnenemy(Transform t)
    {
        audioSource.PlayOneShot(SFXDamage);
        GameObject cloneBlood = Instantiate(blood, t.position, t.rotation);
        Destroy(cloneBlood, 1f);
    }


    // Método cuando muere el soldado
    public void EnemyDeath()
    {
        distanceToPlayer = 0;
        agent.isStopped = true;

        anim.SetTrigger("isDeath");

        audioSource.PlayOneShot(SFXDeath);

        weaponController.GetComponent<SoldierShoot>().StopShoot();
    }


    // Método que realiza una animación de cuando cae el arma al suelo cuando muere el soldado
    void DropTheWeapon()
    {
        Vector3 newPosition = new Vector3(weapon.transform.position.x, 0.1f, weapon.transform.position.z);
        Quaternion newRotation = Quaternion.Euler(2f, 90f, 270f);

        weapon.transform.position = Vector3.Lerp(weapon.transform.position, newPosition, Time.deltaTime * 4f);
        weapon.transform.rotation = Quaternion.Lerp(weapon.transform.rotation, newRotation, Time.deltaTime * 4f);
    }


    // Método para reajustar la puntería del disparo
    void Reappoint()
    {
        Vector3 targetDir = player.transform.position - transform.position;

        Debug.DrawLine(transform.position, targetDir, Color.green);

        Quaternion quaternionTardetDir = Quaternion.LookRotation(targetDir);

        transform.rotation = Quaternion.Lerp(transform.rotation, quaternionTardetDir, Time.deltaTime);
    }
}
