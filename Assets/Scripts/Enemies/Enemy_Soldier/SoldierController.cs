using System.Net.NetworkInformation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class SoldierController : MonoBehaviour
{
    /*
        Controlador del soldador

                             Soldier
        Vida:                   5

        Pts daño:               2
        Pts muerte:            10

        Dist. detección:       20
        Dist. ataque:           7

        Pts daño al jugador    10
    */

    [Header("Game Controller")]
    GameObject gameController;
    GameManager gameManager;


    [Header("Life and points")]
    [SerializeField] private int lifeEnemy;
    int contLifeEnemy = 0;

    [SerializeField] private int pointsDamage;
    [SerializeField] private int pointsDeath;

    bool isDeath = false;

    public bool GetIsDeath() { return isDeath; }


    void Start()
    {
        gameController = GameObject.Find("GameController");
        gameManager = gameController.GetComponent<GameManager>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet") && !isDeath)
        {
            contLifeEnemy++;

            if (contLifeEnemy >= lifeEnemy)
            {
                gameManager.SetScore(pointsDeath);

                isDeath = true;

                GetComponent<SoldierAnim>().EnemyDeath();

                gameManager.SetEnemiesDead();

                Destroy(gameObject, 7f);
            }
            else
            {
                gameManager.SetScore(pointsDamage);

                GetComponent<SoldierAnim>().DamageEnenemy(other.gameObject.transform);
            }

            Destroy(other.gameObject);
        }
    }
}
