using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class SoldierShoot : MonoBehaviour
{
    const float DELAY_SHOOT_RUN = 1.7f;
    const float DELAY_SHOOT_IDLE = 0.8f;


    [SerializeField] private GameObject bullet;


    AudioSource audioSource;
    [SerializeField] private AudioClip sfxShoot;

    float delay = 1.3f;

    bool isShoot = false;


    public void StopShoot() { isShoot = false; }
    public void SetShootIdle(bool isShootIdle) { isShoot = isShootIdle; delay = DELAY_SHOOT_IDLE; }
    public void setShootRun(bool isShootRun) { isShoot = isShootRun; delay = DELAY_SHOOT_RUN; }


    void Start()
    {
        StartCoroutine(Shoot());

        audioSource = GetComponent<AudioSource>();
    }


    IEnumerator Shoot()
    {
        while (true)
        {
            if (isShoot)
            {
                audioSource.PlayOneShot(sfxShoot);

                GameObject cloneBullet = Instantiate(bullet, transform.position, transform.rotation);

                yield return new WaitForSeconds(delay);
            }
            else
            {
                yield return null;
            }
        }
    }
}