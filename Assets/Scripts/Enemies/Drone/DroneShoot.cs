using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class DroneShoot : MonoBehaviour
{
    [Header("Bullet")]
    [SerializeField] private GameObject bullet;


    GameObject player;


    [Header("SFX Shoot")]
    AudioSource audioSource;
    [SerializeField] private AudioClip sfxShoot;


    [Header("Burst and delay")]
    [SerializeField] private int burst = 1;
    [SerializeField] private float delay = 1f;


    bool isShoot = false;

    public void SetShoot(bool shoot) { isShoot = shoot; }


    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        StartCoroutine(Shoot());

        player = GameObject.Find("Player");
    }


    void Update()
    {
        if (isShoot) { Reapuntar(); }
    }


    IEnumerator Shoot()
    {
        while (true)
        {
            if (isShoot)
            {
                int contTirada = 0;

                while (contTirada < burst)
                {
                    audioSource.PlayOneShot(sfxShoot);

                    GameObject cloneBullet = Instantiate(bullet, transform.position, transform.rotation);

                    contTirada++;

                    yield return new WaitForSeconds(0.2f);
                }

                yield return new WaitForSeconds(delay);
            }
            else
            {
                yield return null;
            }
        }
    }


    void Reapuntar()
    {
        Vector3 targetDir = player.transform.position - transform.position;

        Debug.DrawLine(transform.position, targetDir, Color.green);

        Quaternion quaternionTardetDir = Quaternion.LookRotation(targetDir);

        transform.rotation = Quaternion.Lerp(transform.rotation, quaternionTardetDir, Time.deltaTime);
    }
}
