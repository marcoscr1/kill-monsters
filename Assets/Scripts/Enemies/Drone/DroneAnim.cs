using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class DroneAnim : MonoBehaviour
{
    [Header("Weapon")]
    [SerializeField] private GameObject weaponController;

    NavMeshAgent agent;
    [SerializeField] Animator anim;

    GameObject player;


    [Header("Paths")]
    [SerializeField] private GameObject[] pathPoint;
    float distanceToChangePath = 5f;
    int indexPath = 0;


    [Header("Distances of the player")]
    [SerializeField] private float distanceToFollowPlayer;
    [SerializeField] private float distanceToAttackPlayer;
    float distanceToPlayer;


    [Header("SFX")]
    AudioSource audioSource;
    [SerializeField] private AudioClip SFXDeath;
    [SerializeField] private AudioClip SFXDamage;
    [SerializeField] private GameObject fxDamage;


    DroneController droneController;


    void Start()
    {
        agent = GetComponent<NavMeshAgent>();

        player = GameObject.Find("Player");

        audioSource = GetComponent<AudioSource>();

        droneController = GetComponent<DroneController>();
    }


    void Update()
    {
        distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);

        if (distanceToPlayer <= distanceToAttackPlayer && !droneController.GetIsDeath())
        {
            AttackToPlayer();
        }
        else if (distanceToPlayer <= distanceToFollowPlayer && !droneController.GetIsDeath())
        {
            FollowToPlayer();
        }
        else if (pathPoint.Length > 0 && pathPoint != null && !droneController.GetIsDeath())
        {
            FollowToPoint();
        }
    }


    void FollowToPlayer()
    {
        weaponController.GetComponent<DroneShoot>().SetShoot(false);

        agent.SetDestination(player.transform.position);
        agent.stoppingDistance = distanceToAttackPlayer;
    }


    void FollowToPoint()
    {
        weaponController.GetComponent<DroneShoot>().SetShoot(false);

        agent.SetDestination(pathPoint[indexPath].transform.position);
        agent.stoppingDistance = 5;

        if (Vector3.Distance(transform.position, pathPoint[indexPath].transform.position) <= distanceToChangePath)
        {
            indexPath = ((indexPath + 1) == pathPoint.Length) ? 0 : indexPath + 1;
        }
    }


    void AttackToPlayer()
    {
        //anim.SetBool("isShoot", true);
        weaponController.GetComponent<DroneShoot>().SetShoot(true);
    }


    public void DroneDeath()
    {
        audioSource.PlayOneShot(SFXDeath);

        anim.SetBool("isDeath", true);
        weaponController.GetComponent<DroneShoot>().SetShoot(false);
        agent.isStopped = true;

        gameObject.transform.Find("PA_Drone").gameObject.transform.Find("FXExplosion").gameObject.SetActive(true);
        gameObject.transform.Find("PA_Drone").gameObject.transform.Find("SmokeDrone").gameObject.SetActive(true);
    }


    public void DroneDamage(Transform t)
    {
        audioSource.PlayOneShot(SFXDamage);
        GameObject cloneExplosion = Instantiate(fxDamage, t.position, t.rotation);
        Destroy(cloneExplosion, 1f);
    }
}
