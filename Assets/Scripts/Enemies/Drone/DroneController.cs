using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class DroneController : MonoBehaviour
{
    /*
        Controlador del dron

                              DRON
        Vida:                  10

        Pts daño:               5
        Pts muerte:            20

        Dist. detección:       20
        Dist. ataque:          12

        Pts daño al jugador    12
    */

    [Header("Game Controller")]
    GameObject gameController;
    GameManager gameManager;


    [Header("Life and points")]
    [SerializeField] private int lifeEnemy;
    int contLifeEnemy = 0;

    [SerializeField] private int pointsDamage;
    [SerializeField] private int pointsDeath;

    bool isDeath = false;

    public bool GetIsDeath() { return isDeath; }


    void Start()
    {
        gameController = GameObject.Find("GameController");
        gameManager = gameController.GetComponent<GameManager>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet" && !isDeath)
        {
            contLifeEnemy++;

            if (contLifeEnemy >= lifeEnemy)
            {
                gameManager.SetScore(pointsDeath);

                isDeath = true;

                GetComponentInParent<DroneAnim>().DroneDeath();

                gameManager.SetEnemiesDead();

                Destroy(gameObject, 7f);
            }
            else
            {
                gameManager.SetScore(pointsDamage);

                GetComponentInParent<DroneAnim>().DroneDamage(other.gameObject.transform);
            }

            Destroy(other.gameObject);
        }
    }
}
