using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class ZombieSound : MonoBehaviour
{
    float delaySoundWalk;
    float delaySoundRun;
    float delaySoundInitAttack = 0.6f;
    float delaySoundEndAttack = 1.9f;


    [Header("SFX Sound")]
    AudioSource audioSource;
    [SerializeField] private AudioClip SFXDeath;
    [SerializeField] private AudioClip SFXDamage;
    [SerializeField] private AudioClip SFXWalk;
    [SerializeField] private AudioClip SFXRun;
    [SerializeField] private AudioClip SFXAtack;


    private bool stateCorutineWalk = false;
    private bool stateCorutineRun = false;
    private bool stateCorutineAttack = false;


    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }


    public void SoundWalk()
    {
        if (!stateCorutineWalk)
        {
            stateCorutineWalk = true;
            stateCorutineAttack = false;
            stateCorutineRun = false;

            StartCoroutine(SoundCorutineWalk());
        }
    }


    IEnumerator SoundCorutineWalk()
    {
        while (stateCorutineWalk)
        {
            delaySoundWalk = Random.Range(7f, 10f);
            yield return new WaitForSeconds(delaySoundWalk);
            audioSource.PlayOneShot(SFXWalk);

        }
    }


    public void SoundRun()
    {
        if (!stateCorutineRun)
        {
            stateCorutineRun = true;
            stateCorutineAttack = false;
            stateCorutineWalk = false;

            StartCoroutine(SoundCorutineRun());
        }
    }


    IEnumerator SoundCorutineRun()
    {
        while (stateCorutineRun)
        {
            delaySoundRun = Random.Range(2f, 4f);
            yield return new WaitForSeconds(delaySoundRun);
            audioSource.PlayOneShot(SFXRun);
        }
    }


    public void SoundAttack()
    {
        if (!stateCorutineAttack)
        {
            stateCorutineAttack = true;
            stateCorutineWalk = false;
            stateCorutineRun = false;

            StartCoroutine(SoundCorutineAttack());
        }
    }


    IEnumerator SoundCorutineAttack()
    {
        while (stateCorutineAttack)
        {
            yield return new WaitForSeconds(delaySoundInitAttack);

            audioSource.PlayOneShot(SFXAtack);

            yield return new WaitForSeconds(delaySoundEndAttack);
        }
    }


    public void SoundDamage()
    {
        audioSource.PlayOneShot(SFXDamage);
    }


    public void SoundDeath()
    {
        stateCorutineAttack = false;
        stateCorutineWalk = false;
        stateCorutineRun = false;

        StopCoroutine(SoundCorutineWalk());
        StopCoroutine(SoundCorutineRun());
        StopCoroutine(SoundCorutineAttack());

        audioSource.PlayOneShot(SFXDamage);
        audioSource.PlayOneShot(SFXDeath);
    }
}