using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class ZombieController : MonoBehaviour
{
    /*
        Controlador de enemigos

                            Zombie-01
        Vida:                   5

        Pts daño:               2
        Pts muerte:            10

        Dist. detección:       20
        Dist. ataque:           6

        Pts daño al jugador     5
    */

    [Header("Game Controller")]
    GameObject gameController;
    GameManager gameManager;


    [Header("Life and points")]
    [SerializeField] private int lifeEnemy;
    int contLifeEnemy = 0;

    [SerializeField] private int pointsDamage;
    [SerializeField] private int pointsDeath;

    bool isDeath = false;

    public bool GetIsDeath() { return isDeath; }


    void Start()
    {
        gameController = GameObject.Find("GameController");

        gameManager = gameController.GetComponent<GameManager>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet") && !isDeath)
        {
            contLifeEnemy++;

            if (contLifeEnemy >= lifeEnemy)
            {
                gameManager.SetScore(pointsDeath);

                isDeath = true;

                GetComponent<ZombieAnim>().EnemyDeath();

                gameManager.SetEnemiesDead();

                Destroy(gameObject, 7f);

            }
            else
            {
                gameManager.SetScore(pointsDamage);

                GetComponent<ZombieAnim>().DamageEnenemy(other.gameObject.transform);
            }

            Destroy(other.gameObject);
        }
    }
}
