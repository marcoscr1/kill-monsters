using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class ZombieAnim : MonoBehaviour
{
    NavMeshAgent agent;
    [SerializeField] Animator anim;


    GameObject player;
    [SerializeField] GameObject shootController;


    [Header("Speed Zombie")]
    [SerializeField] float speedPoint; // 0.8
    [SerializeField] float speedRunToPlayer; // 2


    [Header("Paths")]
    [SerializeField] private GameObject[] pathPoint;
    float distanceToChangePath = 5f;
    int indexPath = 0;


    [Header("Distances")]
    [SerializeField] private float distanceToFollowPlayer;
    [SerializeField] private float distanceToAttackPlayer;
    float distanceToPlayer;


    [Header("SFX")]
    [SerializeField] private GameObject blood;


    private ZombieController zombieController;
    private ZombieSound zombieSound;


    void Start()
    {
        // Obtenemos las referencias de componentes y GameObjects
        agent = GetComponent<NavMeshAgent>();

        player = GameObject.Find("Player");

        zombieController = GetComponent<ZombieController>();
        zombieSound = GetComponent<ZombieSound>();

        // Ajustamos la distancia donde para el zombie
        agent.stoppingDistance = distanceToAttackPlayer;
    }


    void Update()
    {
        distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);

        if (distanceToPlayer <= distanceToAttackPlayer && !zombieController.GetIsDeath())
        {
            AttackToPlayer();
        }
        else if (distanceToPlayer <= distanceToFollowPlayer && !zombieController.GetIsDeath())
        {
            FollowToPlayer();
        }
        else if (pathPoint.Length > 0 && pathPoint != null && !zombieController.GetIsDeath())
        {
            FollowToPoint();
        }
    }


    // Método cuando el zombie no detecta al jugador y está andando dirección a los puntos establecidos
    void FollowToPoint()
    {
        zombieSound.SoundWalk();

        anim.SetBool("isRun", false);
        anim.SetBool("isAttack", false);

        agent.speed = speedPoint;

        agent.SetDestination(pathPoint[indexPath].transform.position);
        agent.stoppingDistance = 0;

        shootController.GetComponent<ZombieShoot>().SetShoot(false);

        if (Vector3.Distance(transform.position, pathPoint[indexPath].transform.position) <= distanceToChangePath)
        {
            indexPath = ((indexPath + 1) == pathPoint.Length) ? 0 : indexPath + 1;
        }
    }


    // Método cuando el zombie está siguiendo al jugador
    void FollowToPlayer()
    {
        zombieSound.SoundRun();

        anim.SetBool("isRun", true);
        anim.SetBool("isAttack", false);
        agent.speed = speedRunToPlayer;

        agent.SetDestination(player.transform.position);
        agent.stoppingDistance = distanceToAttackPlayer;

        shootController.GetComponent<ZombieShoot>().SetShoot(false);
    }


    // Método cuando el zombie ataca al jugador
    void AttackToPlayer()
    {
        zombieSound.SoundAttack();

        anim.SetTrigger("isAttack");
        shootController.GetComponent<ZombieShoot>().SetShoot(true);
    }


    // Método cuando el zombie sufre un daño
    public void DamageEnenemy(Transform t)
    {
        zombieSound.SoundDamage();

        GameObject cloneBlood = Instantiate(blood, t.position, t.rotation);
        Destroy(cloneBlood, 1f);
    }


    // Método cuando muere el zombie
    public void EnemyDeath()
    {
        distanceToPlayer = 0;
        agent.isStopped = true;

        zombieSound.SoundDeath();

        anim.SetTrigger("isDeath");

        shootController.GetComponent<ZombieShoot>().SetShoot(false);
    }
}