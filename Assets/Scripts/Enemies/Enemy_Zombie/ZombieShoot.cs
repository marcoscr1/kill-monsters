using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class ZombieShoot : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    float delay = 2.63f;

    bool initShoot = false;
    float delayInit = 1.284f;


    bool isShoot = false;


    public void SetShoot(bool shoot) { isShoot = shoot; }


    void Start()
    {
        StartCoroutine(Shoot());
    }


    IEnumerator Shoot()
    {
        while (true)
        {
            if (isShoot)
            {
                if (!initShoot)
                {
                    initShoot = true;
                    yield return new WaitForSeconds(delayInit);
                }
                else
                {
                    GameObject cloneBullet = Instantiate(bullet, transform.position, transform.rotation);

                    yield return new WaitForSeconds(delay);
                }
            }
            else
            {
                initShoot = false;
                yield return null;
            }
        }
    }
}
