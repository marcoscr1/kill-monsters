using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class ZombieBullet : MonoBehaviour
{
    const float SPEED_BULLET = 10f;
    const float LIFE_BULLET = 2f;

    GameObject player;


    void Start()
    {
        Destroy(gameObject, LIFE_BULLET);

        player = GameObject.Find("Player");
    }


    void Update()
    {
        //Calcula la dirección hacia el objetivo
        Vector3 direction = player.transform.position - transform.position;

        // Normaliza la dirección para mantener una velocidad constante
        Vector3 normalizedDirection = direction.normalized;

        // Mueve la bala hacia el objetivo
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, SPEED_BULLET * Time.deltaTime);
    }
}
