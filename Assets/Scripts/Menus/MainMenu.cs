using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class MainMenu : MonoBehaviour
{
    /*
        Controlador del menu principal
    */
    private void Start() {
        Cursor.lockState = CursorLockMode.None;
    }
    
    public void Play()
    {
        SceneManager.LoadScene("Level-01");
    }


    public void Score()
    {
        SceneManager.LoadScene("Score");
    }


    public void Help()
    {
        SceneManager.LoadScene("Help");
    }


    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }


    public void Exit()
    {
        Application.Quit();
    }


    public void ReturnMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
