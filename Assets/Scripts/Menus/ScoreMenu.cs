using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using System.IO;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class ScoreMenu : MonoBehaviour
{
    const string DATA_FILE = "Assets/Resources/data.json";

    [SerializeField] TMP_Text timeTMP;
    [SerializeField] TMP_Text scoreTMP;

    GameData gameData;

    string time;
    int score;


    void Start()
    {
        gameData = LoadData();

        time = gameData.time;

        if (time == "99:99")
        {
            time = "00:00";
        }

        score = gameData.points;
    }


    void Update()
    {
        timeTMP.text = time;
        scoreTMP.text = score.ToString();
    }


    GameData LoadData()
    {
        string fileText = File.ReadAllText(DATA_FILE);
        return JsonUtility.FromJson<GameData>(fileText);
    }
}
