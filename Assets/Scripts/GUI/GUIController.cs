using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class GUIController : MonoBehaviour
{
    // Marcadores
    [SerializeField] private TMP_Text txtLive;
    [SerializeField] private TMP_Text txtEnemiesDead;
    [SerializeField] private TMP_Text txtScore;

    [SerializeField] private Image lifeBar;
    [SerializeField] TMP_Text txtLife;

    GameStates gameStates;


    void Start()
    {
        gameStates = GetComponent<GameStates>();

    }


    void Update()
    {
        txtScore.text = PlayerStates.score.ToString();
        txtEnemiesDead.text = PlayerStates.enemieDead.ToString();
        RemoveLifeBar();
        txtLive.text = PlayerStates.lives.ToString();

    }


    void RemoveLifeBar()
    {
        txtLife.text = PlayerStates.life.ToString();

        float lifeBarNow = (float)PlayerStates.life / 100;
        lifeBar.fillAmount = lifeBarNow;
    }
}
