using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class Timer : MonoBehaviour
{
    /*
        Controlador del tiempo de la partida
    */

    [SerializeField] private TMP_Text textTimer;

    int min = 0, sec = 0;
    bool pauseTimer = true;
    string timeScreem = "00:00";

    public string getTimeGame() { return timeScreem; }


    void Update()
    {
        if (pauseTimer)
        {
            PlayerStates.SetContTime(PlayerStates.contTimeScreen +  Time.deltaTime);

            min = (int) PlayerStates.contTimeScreen / 60;
            sec = (int) PlayerStates.contTimeScreen % 60;

            timeScreem = string.Format("{0:00}:{1:00}", min, sec);

            PlayerStates.SetTime(timeScreem);

            textTimer.text = PlayerStates.timeScreen;
        }
    }


    public void setTimer(bool pause)
    {
        pauseTimer = pause;
    }
}
