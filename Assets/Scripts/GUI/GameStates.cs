using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class GameStates : MonoBehaviour
{
    /*
        Estados del juego: PAUSE, 

    */

    [SerializeField] GameObject controllerScreen;
    bool pause = false;
    [SerializeField] private RectTransform rcPause;

    bool loseLife = false;
    [SerializeField] private RectTransform rcLoseLife;


    void Start()
    {
        pause = false;
        rcPause.gameObject.SetActive(false);
    }


    // Método para pausar la partida
    public void SetPause()
    {
        pause = !pause;

        if (pause)
        {
            rcPause.gameObject.SetActive(true);
            controllerScreen.SetActive(false);

            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
            rcPause.gameObject.SetActive(false);
            controllerScreen.SetActive(true);
        }
    }


    // Método para perder vida
    public void LoseLife()
    {
        if (loseLife)
        {
            loseLife = true;
            rcLoseLife.gameObject.SetActive(true);
            Time.timeScale = 0f;
        }
    }


    IEnumerator CoroutineLoseLife()
    {
        yield return new WaitForSeconds(2f);
        loseLife = false;
        Debug.Log(">reaunudar");
        rcLoseLife.gameObject.SetActive(false);
        Time.timeScale = 1f;
    }


    // Método para salir del juego
    public void ExitGame()
    {
        Debug.Log(">Salir del juego");
    }
}
