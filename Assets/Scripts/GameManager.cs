using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class GameManager : MonoBehaviour
{
    const string DATA_FILE = "Assets/Resources/data.json";
    GameData gameData;


    [Header("Display Drivers")]
    [SerializeField] GameObject controllerScream;
    [SerializeField] RectTransform imageDamage;
    [SerializeField] RectTransform rcLoseLive;
    [SerializeField] GameObject rcGameOver;
    [SerializeField] GameObject rcPause;
    [SerializeField] TMP_Text txtLevel;
    [SerializeField] GameObject rcStartMission;
    [SerializeField] TMP_Text txtStartMission;
    [SerializeField] GameObject rcEndGame;


    [Header("Player Starting Point")]
    [SerializeField] GameObject pointInitial;


    [Header("Player")]
    [SerializeField] GameObject player;

    bool pause = false;

    Dictionary<string, int> levelsDic = new Dictionary<string, int> {
        {"Level-01", 1},
        {"Level-02", 2}
    };

    void Start()
    {
        gameData = LoadData();
        StartMission();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            SetPause();
        }
    }


    // Método para añadir puntuación al jugador
    public void SetScore(int points)
    {
        PlayerStates.SetScore(points);
    }


    // Método para añadir al contador de enemigos eliminados
    public void SetEnemiesDead()
    {
        PlayerStates.SetEnemieDead();
    }


    // Método para eliminar vida al jugador
    public void RemoveLife(int life)
    {
        PlayerStates.RemoveLife(life);

        if (PlayerStates.life <= 0)
        {
            PlayerStates.RemoveLive();

            if (PlayerStates.lives < 0)
            {
                Time.timeScale = 0;
                Cursor.lockState = CursorLockMode.None;
                rcGameOver.SetActive(true);
            }
            else
            {
                PlayerStates.LoseAllLife();
                StartCoroutine(LoseLive());
            }
        }
        else
        {
            StartCoroutine(ActivedDamageImage());
        }
    }

    IEnumerator ActivedDamageImage()
    {
        imageDamage.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        imageDamage.gameObject.SetActive(false);
    }

    IEnumerator LoseLive()
    {
        controllerScream.SetActive(false);

        rcLoseLive.gameObject.SetActive(true);

        Time.timeScale = 0;

        while (!Input.GetKeyDown(KeyCode.Return))
        {
            yield return null;
        }

        PositionInitial();

        PlayerStates.ResetLife();


        rcLoseLive.gameObject.SetActive(false);

        controllerScream.SetActive(true);

        Time.timeScale = 1f;
    }


    // Método para iniciar una nueva partida
    public void NewGame()
    {
        Time.timeScale = 1f;

        PlayerStates.ResetValues();

        SceneManager.LoadScene("Level-01");
    }


    // Método para establecer en pausa el juego
    public void SetPause()
    {
        pause = !pause;

        if (pause)
        {
            Time.timeScale = 0;
            rcPause.SetActive(true);
            controllerScream.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            controllerScream.SetActive(true);
            Cursor.lockState = CursorLockMode.Locked;
            rcPause.SetActive(false);
            Time.timeScale = 1f;
        }
    }


    // Método para colocar al jugador en la posición inicial
    void PositionInitial()
    {
        player.transform.position = new Vector3(pointInitial.transform.position.x, 0.99f, pointInitial.transform.position.z);
        player.transform.rotation = pointInitial.transform.rotation;
    }


    // Metodo para iniciar una mision
    public void StartMission()
    {
        txtLevel.text = levelsDic[SceneManager.GetActiveScene().name].ToString();
        rcStartMission.gameObject.SetActive(true);

        txtStartMission.text = SceneManager.GetActiveScene().name;
        StartCoroutine(StartGameMessage());
    }

    IEnumerator StartGameMessage()
    {
        float delay = 2f;

        yield return new WaitForSeconds(delay);

        rcStartMission.gameObject.SetActive(false);
    }


    // Metodo para cambiar de mision
    // En caso de no haber mas misiones (no hay mas valores en el array levelDiscc)
    // finaliza la partida
    public void ChangeMission()
    {
        // Obtiene el nombre de la escena actual
        string currenteSceneName = SceneManager.GetActiveScene().name;

        // Obtiene el numero de la escena
        int currentLevel = levelsDic[currenteSceneName];

        // Calcula el numero de la proxima fase
        int nextLevel = currentLevel + 1;

        // Comprueba si hay otro nivel
        if (levelsDic.ContainsValue(nextLevel))
        {
            // Busca el nombre de la siguiente fase
            string nextSceneName = levelsDic.FirstOrDefault(x => x.Value == nextLevel).Key;

            // Carga la otra escena
            SceneManager.LoadScene(nextSceneName);
        }
        else
        {
            // Si no hay otro nivel
            if (!CompareTime(gameData.time, PlayerStates.timeScreen))
            {
                SaveData();
            }

            Cursor.lockState = CursorLockMode.None;
            rcEndGame.SetActive(true);
            Time.timeScale = 0;
        }

    }


    // Metodo para volver al menu principal
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }


    // Metodo para cargar los datos del dichero Json
    GameData LoadData()
    {
        string fileText = File.ReadAllText(DATA_FILE);
        return JsonUtility.FromJson<GameData>(fileText);
    }


    // Metodo para guardar los datos en el fichero Json
    void SaveData()
    {
        // Cargamos los datos
        gameData.time = PlayerStates.timeScreen;
        gameData.live = PlayerStates.lives;
        gameData.life = PlayerStates.life;
        gameData.enemyDeath = PlayerStates.enemieDead;

        // Creamos la representación
        string json = JsonUtility.ToJson(gameData);

        // Volcamos los datos al fichero
        File.WriteAllText(DATA_FILE, json);
    }


    // Compara los el tiempo de la partida actual con el tiempo almacenado en el Json
    bool CompareTime(string Htime, string timePlay)
    {
        // Convierte los tiempos a segundos
        string[] arrayHTime = Htime.Split(':');
        string[] arrayTimePlay = Htime.Split(':');

        int segHTime = (int.Parse(arrayHTime[0]) * 60) + int.Parse(arrayHTime[1]);
        int segTimePlay = (int.Parse(arrayTimePlay[0]) * 60) + int.Parse(arrayTimePlay[1]);

        // Compara los tiempos
        return segHTime > segTimePlay;
    }
}
