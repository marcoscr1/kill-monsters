using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFinal : MonoBehaviour
{
    /*
        Controlador de la  luz final de la INTRO
    */

    Light lightt;
    float delay = 6.5f;

    void Start()
    {
        lightt = GetComponent<Light>();
        lightt.range = 0;
        StartCoroutine(Light());
    }

    IEnumerator Light()
    {
        yield return new WaitForSeconds(delay);

        float t = 0;

        while (t < 60)
        {
            lightt.range = t;
            t += Time.deltaTime * 50;
            yield return new WaitForSeconds(0.02f);
        }
    }
}
