using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class IntroController : MonoBehaviour
{
    /*
        Controlador de la INTRO del juego
    */

    const string DATA_FILE = "Assets/Resources/data.json";

    [Header("Screen Controller")]
    [SerializeField] private TMP_Text txtEscapeIntro;
    [SerializeField] private TMP_Text txtInfo;

    [Header("Sounds Intro")]
    [SerializeField] private AudioClip beep;
    [SerializeField] private AudioClip introSound;


    string infoGame = "En el año 2024 un virus ha transformado a criaturas inocentes en monstruos sedientos de destrucción.Explora entornos infectados, descubre los lugares de reproducción de los virus y lucha contra hordas de monstruos.Tu misión es clara: Aniquilar a estas abominaciones y desmantelar los focos de infección.Cada batalla es crucial, cada decisión cuenta.La lucha por la supervivencia ha comenzado!";

    float delay = 0.1f;


    void Start()
    {
        CreateDataFile();

        Cursor.lockState = CursorLockMode.Locked;

        AudioSource.PlayClipAtPoint(introSound, Camera.main.transform.position, 0.5f);

        txtEscapeIntro.enabled = false;

        StartCoroutine(LoadDescription());

        Invoke("EndIntro", 64);
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            EndIntro();
        }
    }


    IEnumerator LoadDescription()
    {
        yield return new WaitForSeconds(11);

        txtEscapeIntro.enabled = true;

        string txtGenerado = "";

        for (int i = 0; i < infoGame.Length; i++)
        {
            if (infoGame[i] == '.')
            {
                txtGenerado += ".\n";
                txtInfo.text = txtGenerado;
                yield return new WaitForSeconds(1.5f);
            }
            else
            {
                txtGenerado += infoGame[i];
                txtInfo.text = txtGenerado + "_";
                AudioSource.PlayClipAtPoint(beep, Camera.main.transform.position, 0.5f);

                yield return new WaitForSeconds(delay);
            }
        }
    }


    void EndIntro()
    {
        SceneManager.LoadScene("MainMenu");
    }


    // Crea el fichero Json de almacenamiento si no existe
    void CreateDataFile()
    {
        if (!File.Exists(DATA_FILE))
        {
            GameData gd = new GameData();
            gd.time = "99:99";

            string json = JsonUtility.ToJson(gd);

            File.WriteAllText(DATA_FILE, json);
        }
    }
}
