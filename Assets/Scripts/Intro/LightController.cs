using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class LightController : MonoBehaviour
{
    /*
        COntrolador de luz de l a intro
    */

    [SerializeField] private float duration; // Luz 1: || Luz 2: 5f;
    [SerializeField] private float delayInit; // Luz 1: || Luz 2: 5f
    [SerializeField] private float endAnim; // Luz 1: 14 || Luz 2: 10.4

    [Header("Sound")]
    [SerializeField] private AudioClip sfxLight;

    Vector3 originalPosition;
    float speed = 2f;

    void Start()
    {
        originalPosition = transform.localPosition;

        StartCoroutine(MoveLight());
    }


    IEnumerator MoveLight()
    {
        yield return new WaitForSeconds(delayInit);

        float t = 0;

        Vector3 finalPosition = new Vector3(endAnim, originalPosition.y, originalPosition.z);

        AudioSource.PlayClipAtPoint(sfxLight, Camera.main.transform.position, 1);

        while (t < duration)
        {
            transform.localPosition = Vector3.Lerp(originalPosition, finalPosition, t / duration);
            t += Time.deltaTime * speed;
            yield return null;
        }

        transform.localPosition = finalPosition;
    }
}
