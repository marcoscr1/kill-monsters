using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

[Serializable]
public class GameData
{
    public string time;
    public int life;
    public int live;
    public int points;
    public int enemyDeath;
}
