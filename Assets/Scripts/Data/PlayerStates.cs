using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class PlayerStates : MonoBehaviour
{
    const int NUM_LIFE = 100;
    const int NUM_LIVES = 3;
    const int SCORE_INITIAL = 0;
    const int ENEMIE_DEAD_INITIAL = 0;


    public static int life { get; private set; } = NUM_LIFE;

    public static int lives { get; private set; } = NUM_LIVES;

    public static int score { get; private set; } = SCORE_INITIAL;

    public static int enemieDead { get; private set; } = ENEMIE_DEAD_INITIAL;

    public static float contTimeScreen { get; private set; } = 0;
    public static string timeScreen { get; private set; } = "00:00";



    public static void RemoveLife(int scoreLife) { life -= scoreLife; }
    public static void SetLife(int scoreLife)
    {
        life += scoreLife;

        if (life > 100) { life = 100; }
    }

    public static void LoseAllLife() { life = 0; }

    public static void ResetLife() { life = 100; }
    public static void SetLive() { lives--; }
    public static void SetScore(int points) { score += points; }
    public static void SetEnemieDead() { enemieDead++; }

    public static void RemoveLive() { lives--; }

    public static void SetContTime(float contTime) { contTimeScreen = contTime; }

    public static void SetTime(string time) { timeScreen = time; }

    public static void ResetValues()
    {
        life = NUM_LIFE;
        lives = NUM_LIVES;
        score = SCORE_INITIAL;
        enemieDead = ENEMIE_DEAD_INITIAL;
        contTimeScreen = 0;
        timeScreen = "00:00";
    }

}
