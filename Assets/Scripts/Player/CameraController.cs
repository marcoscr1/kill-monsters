using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    /*
        Movimiento de la cámara
    */

    const float CLAMP_MIN = -40f;
    const float CLAMP_MAX = 40f;

    float lookSensitivity = 2f;

    Vector2 rotation = Vector2.zero;
    Vector2 smoothRot = Vector2.zero;
    Vector2 velRot = Vector2.zero;

    GameObject player;


    void Start()
    {
        player = transform.parent.gameObject;
    }


    void Update()
    {
        if (Time.timeScale != 0)
        {
            // Giro en X
            player.transform.RotateAround(transform.position, Vector3.up, Input.GetAxis("Mouse X") * lookSensitivity);

            // Giro en Y
            rotation.y += Input.GetAxis("Mouse Y");
            rotation.y = Mathf.Clamp(rotation.y, CLAMP_MIN, CLAMP_MAX);
            smoothRot.y = Mathf.SmoothDamp(smoothRot.y, rotation.y, ref velRot.y, 0.1f);
            transform.localEulerAngles = new Vector3(-smoothRot.y, 0, 0);
        }
    }
}
