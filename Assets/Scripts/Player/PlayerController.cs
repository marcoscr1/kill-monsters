using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    /*
        Movimiento del jugador
    */


    [SerializeField] float speed; // 6
    [SerializeField] float jumpForce; // 5
    [SerializeField] GameManager gameManager;

    [SerializeField] GameObject pointInitial;

    [Header("SFX")]
    AudioSource audioSource;
    [SerializeField] private AudioClip SFXDamage;
    [SerializeField] private AudioClip SFXDeath;

    Rigidbody rb;
    Collider col;

    Dictionary<string, int> enemyBulletsDic = new Dictionary<string, int>() {
        {"SoldierBullet", 10},
        {"DroneBullet", 12},
        {"ZombieBullet", 5},
        {"Wash", 100}
    };


    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        audioSource = GetComponent<AudioSource>();

        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();

        transform.position = new Vector3(pointInitial.transform.position.x, transform.position.y, pointInitial.transform.position.z);
        transform.rotation = pointInitial.transform.rotation;
    }


    void Update()
    {
        // Libera el ratón
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        // Salto
        if (IsGrounded() && Input.GetButtonDown("Jump"))
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }

        // Movimiento del jugador
        Vector2 moveInput = Vector2.zero;
        moveInput.x = Input.GetAxis("Horizontal") * speed;
        moveInput.y = Input.GetAxis("Vertical") * speed;
        moveInput *= Time.deltaTime;
        transform.Translate(moveInput.x, 0, moveInput.y);
    }


    bool IsGrounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, col.bounds.extents.y + 0.9f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (enemyBulletsDic.ContainsKey(other.tag))
        {
            audioSource.PlayOneShot(SFXDamage);

            gameManager.RemoveLife(enemyBulletsDic[other.tag]);

            if (PlayerStates.life <= 0)
            {
                audioSource.PlayOneShot(SFXDeath);
            } else {
                audioSource.PlayOneShot(SFXDamage);
            }

            Destroy(other);
        }
    }
}
