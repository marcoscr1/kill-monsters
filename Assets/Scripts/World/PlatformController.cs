using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class PlatformController : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.transform.SetParent(transform);
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.transform.SetParent(null);
        }
    }
}
