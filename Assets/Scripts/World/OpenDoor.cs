using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/
public class OpenDoor : MonoBehaviour
{
    /*
        Controlador de las puertas moviles
    */

    [SerializeField] Animator animDoor;

    private void OnTriggerEnter(Collider other)
    {
        animDoor.SetTrigger("openDoor");
    }
}
