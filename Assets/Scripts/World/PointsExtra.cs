using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/*
    KILL MONSTERS

    Autor: Marcos Cuadrado Rey

    IES San Clemente (2023-2024)
    Ciclo Superior: DAM-A
    Asignatura: Programacion multimedia y dispositivos moviles
*/

public class PointsExtra : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] TMP_Text txtAddPoints;

    int points = 10;


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            gameManager.SetScore(points);

            StartCoroutine(ScreenAddPoints());
        }
    }

    IEnumerator ScreenAddPoints()
    {
        float delay = 2f;

        txtAddPoints.text = "+" + points.ToString() + " pts.";

        txtAddPoints.gameObject.SetActive(true);

        yield return new WaitForSeconds(delay);

        txtAddPoints.gameObject.SetActive(false);

        Destroy(transform.parent.gameObject);
    }

}
