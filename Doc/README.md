# GAME DESIGN DOCUMENT

![Title](img/title.png)

**Título del juego:** Kill Mosters

**Creado por:** Marcos Cuadrado Rey

**Plataforma:** Mac OS M1

**Versión del documento:** 1.0

## Historial de versiones

| **Versión** | **Fecha** | **Comentarios** |
| --- | --- | --- |
| 1.0 |05/05/2024 | Primera versión del documento |

## Presentación

### Título del juego

**Kill Monsters**

### Concepto
**Kill Monsters** es un frenético juego de disparos en primera persona situado en un mundo apocalíptico infestado de zombies y soldados alienígenas. Los jugadores se enfrentarán a hordas de enemigos mientras exploran pasillos llenos de trampas y recompensas, con el objetivo de completar misiones y así poder terminar con esta invasión.

### Puntos clave
- **Combate Intenso:** Experimenta la adrenalina de enfrentarte a oleadas de enemigos en combates rápidos y frenéticos.
-  **Exploración y Estrategia:** Descubre pasillos llenos de trampas mortales y recompensas, que te obligarán a planificar tu camino con astucia.
-  **Variedad de Enemigos:** Enfréntate a una amplia gama de amenazas, desde hordas de zombies hasta soldados alienígenas con habilidades únicas.
-  **Progresión a través de Misiones:** Completa emocionantes misiones que te llevarán a través de diversos entornos apocalípticos, cada uno con sus propios desafíos y peligros.

### Género
**Kill Monsters** está dirigido a aficionados a los juegos de disparos en primera persona **FPS** con un enfoque en la acción intensa y la estrategia táctica. Este juego atraerá especialmente a aquellos que disfrutan de retos desafiantes y una experiencia de juego emocionante. Los aficionados a los juegos similares a Quake, Doom y Half-Life encontrarán **Kill Monsters** irresistible.

### Público Objetivo
**Kill Monsters** está diseñado para jugadores mayores de **16 años**, tanto hombres como mujeres, que disfrutan de la acción rápida y la estrategia táctica en los juegos de disparos en primera persona. Este juego apunta a un público que busca una experiencia de juego emocionante y desafiante, con un enfoque en la superación de obstáculos y la mejora de habilidades a lo largo del juego.

### Experiencia de Juego 
Los jugadores de **Kill Monsters** experimentarán una emocionante aventura llena de acción y peligro mientras se sumergen en un mundo apocalíptico infestado de enemigos. Verán pasillos oscuros y claustrofóbicos llenos de trampas mortales y sorpresas ocultas, mientras escuchan los aterradores sonidos de los zombies y los disparos de los soldados alienígenas. Utilizando su habilidad y estrategia, los jugadores podrán enfrentarse a una variedad de enemigos desafiantes y completar misiones emocionantes para avanzar en la historia y descubrir los secretos de este mundo devastado.

## Diseño

-  **Ambientación Inmersiva:** Sumergir al jugador en un mundo apocalíptico lleno de detalles y atmósfera intensa. Se logrará mediante el diseño de escenarios detallados, efectos visuales y sonoros realistas, y eventos dinámicos que mantengan la tensión en todo momento.
-  **Acción Frenética:** Proporcionar una experiencia de juego emocionante y llena de acción. Esto se logrará mediante el diseño de encuentros con enemigos bien equilibrados, movimientos fluidos y una variedad de armas y habilidades que mantengan a los jugadores en constante acción.
- **Variedad de Desafíos:** Ofrecer una amplia gama de desafíos y obstáculos para mantener el interés del jugador. Esto se logrará mediante la introducción de diferentes tipos de enemigos con comportamientos únicos y trampas ingeniosas que requieran estrategia y habilidad para superar.
-  **Progresión Gratificante:** Hacer que la progresión del jugador a lo largo del juego sea satisfactoria y gratificante. Esto se logrará mediante la implementación de sistemas de recompensas que motivará al jugador a avanzar en la historia.

## Mecánicas del juego

### Núcleo de Juego

#### Jugador
El jugador tendrá que desplazarse por pasillos y habitaciones con una temática apocalíptica. Tendrá la capacidad de disparar, por lo que tendrá que enfrentarse a los enemigos durante todo el juego.

#### Enemigos
Los enemigos se encontrarán a lo largo del recorrido del juego. Unos estarán deambulando, como los zombies, y otros en actitud vigilante. Cada uno tiene la capacidad de disparar al jugador, así mismo también tienen diferentes niveles de vida.

#### Combate
Los combates se realizarán mediante disparos. Cada enemigo tiene un distinto nivel de vida, por lo que el jugador tendrá diversas dificultades para acabar con ellos.

#### Exploración
El jugador podrá moverse a través de pasillos y habitaciones para encontrar recursos y avanzar en las misiones.

#### Trampas y recompensas
A lo largo del recorrido del mundo del juego, el jugador se encontrará con diversas recompensas y trampas

### Iteracciones

#### Puertas automáticas
Durante el recorrido de pasillos y habitaciones, aparecerán puertas cerradas que al acercarse el jugador se abrirán automáticamente.

![Main Menu](img/door.png)

#### Plataformas móviles
El jugador se encontrará con distintas plataformas móviles que podrán desplazar al jugador de un lugar a otro. Normalmente el jugador utilizará estas plataformas para esquivar trampas.

![Main Menu](img/platform2.png)

### Progresión

#### Completar misiones
Los jugadores avanzan completando misiones que implican en llegar desde el punto de inicio hasta el punto de fin.

### Flujo de juego
1. El jugador inicia la misión en un punto de partida.
2. Se mueve a través de los pasillos enfrentándose a enemigos y evitando trampas.
3. Recoge recompensas.
4. Llega al punto de fin de la misión para completarla.

### Fin del juego
- **Derrota:** El jugador muere al perder toda su vida.
- **Victoria:** El jugador llega con éxito al punto de fin de la misión.
- **Otros:** El jugador puede salir del juego en cualquier momento desde el menú principal.

### Física de Juego
La física del juego se aplica a la trayectoria de las balas, la gravedad que afecta a los objetos y personajes, y la detección de colisiones entre el jugador, los enemigos y el entorno.

### Controles
| **Acción** | **Control** |
| --- | --- |
| Desplazarse | Teclas de movimiento |
| Visión | Movimiento del ratón |
| Disparar | Botón izquierdo del ratón |
| Menú/Pausa | Tecla **P** |

## Mundo del juego

### Descripción General
El mundo de **Kill Monsters** se sumerge en un paisaje apocalíptico, marcado por la devastación y la desolación. Los escenarios están dominados por estructuras deterioradas y pasillos oscuros que evocan una sensación de abandono y desesperación.

La iluminación es escasa y sombría, con destellos ocasionales de luz provenientes de antorchas parpadeantes o esporádicos destellos eléctricos. Esta atmósfera ominosa crea un ambiente tenso y claustrofóbico, donde cada rincón puede albergar una nueva amenaza.

El jugador se encuentra inmerso en este mundo desolado, armado solo con un arma de fuego y su ingenio para enfrentarse a las innumerables amenazas que lo rodean. La sensación de vulnerabilidad se acentúa por la limitada visión del jugador, que se ve restringida a la perspectiva en primera persona y al campo de visión estrecho centrado en su arma. Esta limitación aumenta la tensión y la incertidumbre, ya que el jugador nunca sabe qué horrores pueden aparecer repentinamente frente a él.

En resumen, el mundo de **Kill Monsters** es un lugar sombrío y peligroso, donde la supervivencia depende de la habilidad del jugador para adaptarse y enfrentarse a los desafíos implacables que acechan en cada esquina.

### Personajes

#### Jugador
Al tratarse de un juego **FPS**, la visión del mundo y la interacción con el entorno es desde una perspectiva en primera persona, por lo que no se verá al jugador. En la pantalla aparecerá el arma del jugador y ésta tendrá una mira laser con la que podrá apuntar a los enemigos.

Cada vez que el jugador dispare, el arma tendrá un efecto de retroceso y un efecto visual y sonoro de disparo. El arma utilizada ha sido descargada de [Unity Asset Store](https://www.assetstore.unity.com) de un paquete llamado **Handgun**.

![Player](img/player.png)

**Clases que controlan a los Zombies**

```mermaid
graph LR
A((Player)) --> B[PlayerController.cs]
A --> C((Visor))
A --> D((Main Camera))
D --> E[WeaponController]
E --> F((WeaponController.cs))
E --> G[Weapon]
G --> H[BulletController.cs]
G --> I((Mirror))
```

#### Enemigos
**Zombies:** Los zombies se encuentran deambulando por los pasillos y tienen la capacidad de detectar al jugador. Una vez detectado al jugador, se aproximarán a él y lanzará bolas de energías que causará daño al jugador.

Hay tres tipos de zombies, los cuales han sido descargados de la página web de [mixamo](https://www.mixamo.com/), así como sus animaciones.

|**Warzombie F Pedroso** | **Zombiegirl W Kurniawan** | **Romero** |
| --- | --- | --- |
|  ![Warzombie F Pedroso](img/warzombie_f_pedroso.png) | ![Zombiegirl W Kurniawan](img/zombiegirl_w_kurniawan.png)| ![Romero](img/romero.png)|

**Clases que controlan a los Zombies**

```mermaid
graph LR
A((Zombie)) ----> B[ZombieController.cs]
A ----> C[ZombieAnim.cs]
A ----> D[ZombieBullet.cs]
A ----> E[ZombieSound.cs]
A ----> F((WaeponController)) --> G[WeaponController.cs]
F --> H((Bullet))
H --> I[BulletController.cs]
```

| **Participación** | **Nivel de vida** | **Ataque** | **Nivel de daño** |
| --- | --- | --- | --- |
| Abundante | Bajo | Bola de energía | Bajo |

---

**Soldados Alienígenas:** Los soldados alienígenas se encuentran en puntos concretos de los pasillos con una actitud de vigilancia. Su forma de ataque es disparar un arma laser. Cuando detectan al jugador, saldrán corriendo hacia él. Los soldados alienígenas tienen la capacidad de realizar disparos a distinta velocidad según en que situación se encuentre.

Este soldado alienígena ha sido descargado de la página web de [mixamo](https://www.mixamo.com/), así como sus animaciones.

|**Alien Soldier** |
| --- |
|  ![Alien Soldier](img/alien_soldier.png) |

**Clases que controlan a los Soldados Alienígenas**

```mermaid
graph LR
A((Soldado Alienígena)) ----> B[SoldierController.cs]
A ----> C[SoldierAnim.cs]
A ----> D[SoldierBullet.cs]
A ---> E((WaeponController)) --> F[WeaponController.cs]
E --> G((Bullet))
G --> H[BulletController.cs]
```

| **Participación** | **Nivel de vida** | **Ataque** | **Nivel de daño** |
| --- | --- | --- | --- |
| Medio| Medio | Disparos | Medio |

---

**Drones:** Los drones se encuentran sobrevolando por los pasillos en actitud de vigilancia. Cuando detectan al jugador se aproximarán a él y una vez se encuentran a una distancia comenzarán a realizar ráfagas de disparos.

El drone ha sido descargado de [Unity Asset Store](https://www.assetstore.unity.com) de un paquete llamado **SciFi Enemies and Vehicles**.

|**Drone** |
| --- |
|  ![Drone](img/drone.jpg) |

**Clases que controlan a los Drones**

```mermaid
graph LR
A((Drone)) ----> B[DroneController]
A ----> C[DroneAnim]
A ----> D[DroneBullet]
A ---> E((WaeponController)) --> F[WeaponController.cs]
E --> G((Bullet))
G --> H[BulletController.cs]
```

| **Participación** | **Nivel de vida** | **Ataque** | **Nivel de daño** |
| --- | --- | --- | --- |
| Bajo | Alto | Disparos | Alto |

### Objetos

Hay dos tipos de objetos en el juego:

- **Power Ups**: Pack descargado de Unity Assets llamado **Simple Collectibles Pack**
- **Decorativos**: Pack descargado de Unity Assets llamado **Sci-F PBR Prop**

#### Aumentar vida
Cuando el jugador atraviesa este *power up*, éste desaparece y aumenta la vida del jugador.

![Add Life](img/addLife.png)

#### Puntuación extra
Cuando el jugador atraviesa este *power up*, éste desaparece y aumenta la puntuación del jugador.

![Add Score](img/addScore.png)
### Flujo de Pantallas

```mermaid
graph LR
A[Intro] --> B[Main Menu]
B --> C[Game]
C --> D[Mission-01]
D --> E[Mission-02]
B --> F[Puntuaciones]
B --> G[Ayuda]
B --> H[Créditos]
B --> I[Salir]
```

#### Intro
Pequeña introducción donde se aparece el título del juego con un juego de luces, sombras y sonidos. Una vez que aparece el título del juego, comienza a aparecer una breve descripción de la historia de la aventura.

![Intro](img/intro.png)

De esta escena se pasará automáticamente al menú principal, ya sea pasado un tiempo establecido o pulsando la tecla **Esc**.

```mermaid
sequenceDiagram
Intro ->> Main Menú: Pasado un tiempo
Intro ->> Main Menú: Tecla [Esc]
```

#### Main Menú

El menú principal del juego tiene los siguientes enlaces:

- **Jugar**: Comienza una nueva partida.
- **Puntuaciones**: Presenta la tiempo mas rápido de una partida finalizada.
- **Ayuda**: Muestra los controles para interaccionar con el jugador.
- **Créditos**: Muestra los datos del autor del juego.
- **Salir**: Sale del juego.

![Main Menu](img/main_menu.png)

#### Jugar

El juego está dividido en misiones (escenas). Cuando el jugador finaliza la misión, automáticamente cambiará a la siguiente.

Durante el juego aparecerán diferentes imágenes según las siguientes situaciones:

- **Inicio de la misión**: Cuando el jugador comienza una misión, la imagen del juego aparecerá con un color azul y con el nombre de la misión. Esta imagen desaparecerá automáticamente pasado unos segundos y el jugador comenzará la misión.

![Start Mision](img/start_level.png)

- **Daño al jugador**: Cuando el jugador recibe un daño, aparecerá en el borde de la pantalla un efecto de sangre. Esta imagen desaparecerá automáticamente pasado unos segundos.

![Game](img/damage.png)

- **Puntuación extra**: Cuando el jugador adquiere el *power up* de puntuación extra, aparecerá en la pantalla la cantidad de puntos que ha ganado el jugador. Esta imagen desaparecerá automáticamente pasado unos segundos.

![Score Extra](img/points_game.png)

- **Pausa**: Cuando el jugador pulsa la tecla **P** se pausará el jugador. La pantalla cambiará a un color azul indicando que se está en modo *pausa* y dos botones:
-- **Continuar**: Para regresar a la partida y continuar jugando.
-- **Main Menú**: Regresa al menú principal.

![Pause](img/pause.png)

- **Perder una vida**: Cuando el jugador pierde una vida, el juego se detendrá y aparecerá la imagen del juego de color rojizo con un mensaje de para continuar pulses la tecla **Intro**. Una vez se pulse la tecla, el jugador volverá al punto de partida de la misión en la que se encuentre.

![Lose Live](img/loose_file.png)

- **Game Over**: Cuando el jugador pierde todas sus vidas, finalizará el juego. El juego se detendrá y aparecerá la imagen con color rojizo con dos botones:
-- **Menú principal**: Vuelve al menú principal.
-- **Nueva partida**: Vuelve a empezar el juego desde el inicio.

![Game Over](img/game_over.png)

- **Game Over**: Cuando el jugador finaliza todas las misiones, finalizará el juego. El juego se detendrá y aparecerá la imagen con color azul con dos botones:
-- **Menú principal**: Vuelve al menú principal.
-- **Nueva partida**: Vuelve a empezar el juego desde el inicio.

![End Game](img/end.png)

#### Puntuaciones

En esta escena mostrará el mejor tiempo con el que se finalizó una partida, así como sus puntos obtenidos.

![Score](img/puntuaciones.png)

#### Ayuda

En esta escena se mostrarán los controles del juego.

![Help](img/help.png)

#### Créditos

En esta escena muestra la informador del desarrollador del juego.

![Credits](img/credits.png)

### HUD

![HUD](img/hub.png)

El HUD del juego muestra la siguiente información:

- **Puntuación**
![Score](img/points.png)

- **Duración de la partida**
![Time Game](img/time.png)

- **Misión en la que está el jugador**
![Level](img/level.png)

- **Número de vidas**
![Lives](img/cont_live.png)

- **Vida restante del jugador**
![Life](img/cont_life.png)

- **Número de enemigos eliminados**
![Dead Enemies](img/cont_enemies_death.png)

## ARTE

### Metas de Arte

#### Estilo de Arte
El objetivo principal del arte en **Kill Monsters** es crear una atmósfera inmersiva y atmosférica que refleje la desolación y la peligrosidad del mundo apocalíptico del juego. El estilo de arte se caracteriza por ser realista pero con un toque de fantasía. Se busca una estética que combine elementos oscuros y sombríos con destellos de color vibrante para resaltar los peligros y las criaturas que acechan en las sombras.

#### Apariencia General de Escenarios
Los escenarios están diseñados para transmitir una sensación de desolación y abandono. Los pasillos oscuros reflejan la decadencia de una civilización que ha sido devastada por el apocalipsis. Se busca crear una ambientación claustrofóbica y opresiva, donde el jugador se sienta constantemente amenazado por el entorno hostil que lo rodea.

#### Apariencia General de Personajes
Los personajes en **Kill Monsters** son representaciones vívidas de la desesperación y la lucha por la supervivencia en un mundo devastado. Los zombies están deformados y descompuestos, con piel pálida y ojos vacíos que reflejan su estado de no-muertos. Los soldados alienígenas, por otro lado, son criaturas biomecánicas con armaduras avanzadas y armas letales, diseñadas para infundir miedo y terror en sus enemigos. Se busca crear una apariencia distintiva para cada tipo de enemigo, utilizando detalles visuales para resaltar sus características únicas y su amenaza potencial para el jugado.

### Assets de Arte

#### Jugador
- Se mueve por toda la escena del juego
- Salta
- Dispara
- Emite sonidos cuando recibe daños del enemigo o muere

#### Arma del jugador
- Retroceso
- Sonido en el disparo
- Efecto visual en el disparo
- Ráfaga de la bala

#### Zombies
- Se mueven por puntos establecidos en el escenario
- Detectan al jugador
- Emiten sonidos
- Disparan bolas de energía
- Sus animaciones son:
-- Andar
-- Correr
-- Atacar

#### Soldados alienígenas
- Se mueven por puntos establecidos en el escenario
- Detectan al jugador
- Disparan con un arma láser
- Sus animaciones son:
-- Andar
-- Correr
-- Disparar

#### Drones
- Están volando por puntos establecidos en el escenario
- Detectan al jugador
- Disparan ráfagas con un arma
- Sus animaciones son:
-- Balanceo durante el vuelo
-- Cuando son destruidos: Explotan, caen al suelo y desprende humo


## AUDIO

### Metas de Audio

#### Objetivo General
El audio en **Kill Monsters** juega un papel crucial en la creación de una experiencia inmersiva y emocionante para el jugador. El objetivo principal es sumergir al jugador en el mundo apocalíptico del juego, aumentar la tensión y la emoción durante los momentos de acción, y crear una atmósfera envolvente que refleje la desolación y el peligro que acecha en cada esquina. Se busca lograr una experiencia auditiva que complemente y mejore la jugabilidad y la narrativa del juego.

#### Concepto General de la Música
La música en **Kill Monsters** está diseñada para intensificar la emoción y la tensión durante los momentos clave del juego, como los encuentros con enemigos y los momentos de peligro inminente. Se utilizarán composiciones musicales orquestales y electrónicas, con tonos oscuros y melodías inquietantes que se adaptan al ambiente apocalíptico del juego.

#### Efectos de Sonido
Los efectos de sonido en **Kill Monsters** son fundamentales para crear una experiencia auditiva auténtica y envolvente. Se utilizarán efectos realistas y detallados para representar los diferentes elementos del juego, como disparos de armas, gruñidos de zombies y explosiones.

### Assets de Audio

#### Música
- Sonido ambiente para el Main Menu: *intro_menu.mp3*

#### Sonidos
- **Intro**
-- SOnido ambiente: *SoundIntro.mp3*
-- Efecto aire: *SXFAir-01.mp3* y *SXFAir-02.mp3*
-- Beep teclado: *Beep teclado.mp3*

- **Jugador**
-- Recibe un daño: *player_damage.mp3*
-- Muerte del jugador: *player_death.mp3*
-- Disparo del jugador: *player_shoot.mp3*

- **Zombies**
-- Caminando: *zombie_walk.mp3*
-- Corriendo: *zombie_run.mp3*
-- Daño del zombie: *zombie_damage.mp3*
-- Muerte del zombie: *zombie_death.mp3*
-- Disparo de bola de energía: *zombie_shoot.mp3*

- **Soldados alienígenas**
-- Daños del soldado: *soldier_damage.mp3*
-- Muerte del soldado: *soldier_death.mp3*
-- Disparo: *soldier_shoot.mp3*

- **Drones**
-- Daños al dron: *drone_damage.mp3*
-- Destrucción del dron: *drone_explosion.mp3*
-- Disparo: *drone_shoot.mp3*

## DETALLES TÉCNICOS

### Plataformas Objetivo
El juego en un primer momento será publicado para la plataforma **Mac OS X**.

### Herramientas de Desarrollo

#### Motor del juego
**Software:** Unity v2022.3.10f1

**Complementos**

#### Imagen
**Software:** Adobe Photoshop CC 2019

#### Diseño 3D
**Software:** Blender
 
- Diseño de las letras en 3D con el nombre del juego. Estas letras son utilizadas en la animación de presentación del juego.

#### Música
**Software:** Audacity

- Modificación del tiempo de los audios para adaptarlos a la dinámica del juego

#### Recursos
- **Fran Montoiro:** Video-temario del profesor Fran Montoiro.
[https://www.youtube.com/@fran-SC](https://www.youtube.com/@fran-SC)

- **Mixamo:** Web donde se han descargado los personas 3D, así como sus animaciones.
[www.mixamo.com](www.mixamo.com)

- **Luis Canary:** Canal de YoutTube con numerosos tutoriales de UNITY.
[https://www.youtube.com/@LuisCanary](https://www.youtube.com/@LuisCanary])

- **@alexproductionsnocopyright:** Canal de YoutTube con música free. De este canal se extrajo la canción **ELITE** utilizada para el fondo para el menú principal
[https://www.youtube.com/channel/UCx0_M61F81Nfb-BRXE-SeVA](https://www.youtube.com/channel/UCx0_M61F81Nfb-BRXE-SeVA)





